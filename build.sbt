

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor_2.11" % "2.4.1",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4.1" % "test",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test")

fork in run := true

fork in run := true