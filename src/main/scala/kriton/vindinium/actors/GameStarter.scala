package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile._
import scala.util.Random
import akka.actor.ActorRef
import kriton.vindinium.Server
import akka.util.Timeout
import scala.concurrent.duration._
import akka.actor.ReceiveTimeout
import java.util.concurrent.TimeoutException
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._

class GameStarter(server: Server, gameType: String, turns: Int, timeout: Int) extends Actor {
  import GameStarter._
  import context._

  val input = gameType match {
    case "arena" => server.arena
    case _       => server.training(turns, None)
  }

  println(s"View url for new $gameType game is: ${input.viewUrl}")

  actorOf(GameRunner.props(server, input, timeout), "run")

  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
    case _ => Escalate
  }

  def receive = {
    case x => parent forward x
  }

}

object GameStarter {
  def props(server: Server, gameType: String, turns: Int, timeout: Int) = Props(new GameStarter(server, gameType, turns, timeout))

  case object Step
}