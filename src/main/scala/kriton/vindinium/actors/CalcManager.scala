package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile
import kriton.vindinium.Tile._
import kriton.vindinium.Pos
import scala.util.Random
import akka.util.Timeout
import scala.concurrent.duration._
import akka.actor.ActorRef
import akka.actor.Cancellable

class CalcManager(input: Input) extends Actor {
  import context._
  import CalcManager._
  import input.game.board

  var counter = 0
  Dir.values.foreach { decideOn }

  def receive = {
    case Spawn(input, movement, depth) => { counter += 1; context.actorOf(ScoreCalculator.props(input, parent, movement, depth), "" + counter) }
    case SpawnSchedule(spawn)          => system.scheduler.scheduleOnce(3 milliseconds, self, spawn)
  }

  def decideOn(dir: Dir.Dir) = {
    if (board at input.hero.pos.to(dir) exists (_ != Wall)) {
      counter += 1
      actorOf(ScoreCalculator.props(input, parent, dir), "" + counter)
    }
  }

  //  override def postStop() {
  //    println(s"Killing $counter scorers")
  //    super.postStop()
  //  }
}

object CalcManager {
  def props(input: Input) = Props(new CalcManager(input))

  case class Spawn(input: Input, movement: Dir, depth: Option[(Int, Dir, Set[Pos], Pos, Int, Map[Int, Set[Pos]])])
  case class SpawnSchedule(spawn: Spawn)
}