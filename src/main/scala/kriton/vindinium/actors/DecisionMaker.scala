package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile
import kriton.vindinium.Tile._
import kriton.vindinium.Pos
import scala.util.Random
import akka.util.Timeout
import scala.concurrent.duration._
import akka.actor.ActorRef
import scala.collection.mutable.Map
import akka.actor.Cancellable

class DecisionMaker(input: Input, history: Seq[Dir.Dir], timeLimit: Int) extends Actor {
  import context._
  import DecisionMaker._
  import input.game.board

  val scores = Map[Dir.Dir, (Int, Pos, Tile, Int)]()
  val time = System.currentTimeMillis()
  system.scheduler.scheduleOnce(timeLimit milliseconds, self, TakeDecisionCommand)

  actorOf(CalcManager.props(input), "workers")

  def receive = {
    case Score(value, dir, pos, tile, moves) => updateScores(dir, value, pos, tile, moves)
    case TakeDecisionCommand if !scores.isEmpty => {
      //      scores foreach { x => println(s"score for ${x._1} is ${x._2}") }
      val decision = scores.maxBy(_._2._1)
      //      println(s"decision is ${decision._1}")
      parent ! SWBot.Decision(decision._1)
      //      println(s"Taking decision after ${System.currentTimeMillis() - time}")
      stop(self)
    }
    case TakeDecisionCommand => {
      parent ! SWBot.Decision(Stay)
      //      println(s"Taking decision after ${System.currentTimeMillis() - time}")
      stop(self)
    }
  }

  def updateScores(dir: Dir.Dir, score: Int, finalPos: Pos, target: Tile, moves: Int) = {
    if (scores.contains(dir)) {
      scores += dir -> (scores.get(dir).get._1 max score, finalPos, target, moves)
    } else
      scores += dir -> (score, finalPos, target, moves)
  }

}

object DecisionMaker {
  def props(input: Input, history: Seq[Dir.Dir], timeLimit: Int) = Props(new DecisionMaker(input, history, timeLimit))

  case class Score(score: Int, movement: Dir, finalPos: Pos, target: Tile, moves: Int)
  case object TakeDecisionCommand
}