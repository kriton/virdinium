package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile._
import scala.util.Random
import akka.actor.ActorRef
import kriton.vindinium.Server
import akka.util.Timeout
import scala.concurrent.duration._
import akka.actor.ReceiveTimeout
import java.util.concurrent.TimeoutException
import kriton.vindinium.Board
import kriton.vindinium.Pos

class GameRunner(server: Server, initialInput: Input, timeout: Int) extends Actor {
  import GameRunner._
  import context._

  setReceiveTimeout(775 milliseconds)

  var input = initialInput

  val bot = actorOf(SWBot.props(input), "bot")
  self ! Step(100)

  def receive = {
    case ReceiveTimeout     => throw new TimeoutException("GameRunner timeout after 775 milliseconds...")
    case Step(timeLimit)    => step(timeLimit)
    case direction: Dir.Dir => move(input.playUrl, direction)
  }

  def move(url: String, dir: Dir.Value) = {
    input = server.move(input.playUrl, dir)
    self ! Step(timeout)
  }

  def step(timeLimit: Int) = {
    if (!input.game.finished) {
      print(".")
      bot ! SWBot.MoveCommand(input, timeLimit)
    } else {
      parent ! GameMatcher.GameEnded(input)
      stop(self)
    }
  }

}

object GameRunner {
  def props(server: Server, initialInput: Input, timeout: Int) = Props(new GameRunner(server, initialInput, timeout))

  case class Step(timeLimit: Int)
}