package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile
import kriton.vindinium.Tile._
import kriton.vindinium.Pos
import kriton.vindinium.Board
import scala.util.Random
import akka.util.Timeout
import scala.concurrent.duration._
import akka.actor.ActorRef

class ScoreCalculator(input: Input, mapper: ActorRef, movement: Dir, depth: Option[(Int, Dir, Set[Pos], Pos, Int, Map[Int, Set[Pos]])] = None) extends Actor {
  import context._
  import ScoreCalculator._
  import input.game.board
  import CalcManager._

  var possibleHeroPositions: Map[Int, Set[Pos]] = if (depth == None) Map().withDefault { x => Set() } else depth.get._6
  val depthCount = if (depth == None) 0 else depth.get._1
  val heroStats = input.game.heroes.map(h => h.id -> (h.life - depthCount max 1, h.mineCount)) toMap
  val enemyLocs = input.game.heroes.map(h => h.id -> h.pos) filter (h => h._1 != input.hero.id) toMap
  val mylife = input.hero.life - depthCount max 1
  val mymines = input.hero.mineCount
  val from = if (depth.isEmpty) input.hero.pos else depth.get._4
  val maxMoves = board.size * 2

  def positionByTavern(pos: Pos) = pos.neighbors.map(x => board at x).filter(_.contains(Tavern)).isEmpty
  def enemyByTavern(id: Int) = !enemyLocs(id).neighbors.map(x => board at x).filter(_.contains(Tavern)).isEmpty

  board at from.to(movement) match {
    case Some(Air)                           => actOn(Air, from.to(movement), checkNeighbours(from.to(movement)) / (depthCount + 1) + tavernBonus(from.to(movement)) / (depthCount + 2))
    case Some(PossibleHero(l, x))            => actOn(Air, from.to(movement), checkNeighbours(from.to(movement)) / (depthCount + 1) + tavernBonus(from.to(movement)) / (depthCount + 2))
    case Some(Mine(m: Option[Int]))          => actOn(Mine(m), from, tavernBonus(from) + checkNeighbours(from) + mine(board.size, depthCount, m.getOrElse(0), input.hero.id, mylife, input.hero.mineCount))
    case Some(Tavern)                        => actOn(Tavern, from, tavern(board.size, depthCount, mylife))
    case Some(Hero(x)) if x != input.hero.id => actOn(Hero(x), from, tavernBonus(from) + checkNeighbours(from) + hero(board.size, depthCount, mylife, heroStats(x)._1, heroStats(x)._2, enemyByTavern(x), mymines))
    case target                              => //println(s"IGNORING ${target}.")
  }

  def actOn(target: Tile, newpos: Pos, score: Int) {
    var updatedScore: Int = score + (if (depth.isEmpty) 0 else depth.get._5)
    //    println(s"Acting on '${target}' for ${movement} from ${from}. Score is ${score}. Hero id is ${input.hero.id}. Distance is $depthCount")
    val historySet = if (depth.isEmpty) Set[Pos]() + newpos else depth.get._3 + newpos
    if (target != Air && !target.isInstanceOf[PossibleHero])
      mapper ! DecisionMaker.Score(score, if (depth.isEmpty) movement else depth.get._2, from.to(movement), board at from.to(movement) get, depthCount)

    if (movement != Dir.Stay && from != newpos && (depth.isEmpty || depth.get._1 < maxMoves) && checkNeighbours(newpos) >= 0) {
      val prevDepth = if (depth.isEmpty) (0, movement, historySet, newpos, updatedScore, possibleHeroPositions) else depth.get
      Dir.values filter (_ != Dir.Stay) map (e => (e, newpos.to(e))) filter { e => e._2.isIn(board.size) && !historySet.contains(e._2) } foreach { x =>
        parent ! SpawnSchedule(Spawn(updateInput(), x._1, Some(prevDepth._1 + 1, prevDepth._2, historySet, newpos, updatedScore, possibleHeroPositions)))
      }
    }
  }

  stop(self)

  def receive = {
    case _ =>
  }

  def tavernBonus(myLoc: Pos): Int = if (!myLoc.neighbors.map(x => board at x).filter(_.contains(Tavern)).isEmpty) {
    tavern(board.size, depthCount + 1, mylife) / 4
  } else 0

  def checkNeighbours(myLoc: Pos): Int = {
    myLoc.neighbors.filter { x => x isIn (board.size) }.filter { x => board.at(x).get.isInstanceOf[Hero] || board.at(x).get.isInstanceOf[PossibleHero] }
      .map { x => (board.at(x).get, x) }.map { x => if (x._1.isInstanceOf[Hero]) (x._1.asInstanceOf[Hero].id, true, x._2) else (x._1.asInstanceOf[PossibleHero].id, false, x._2) }.filter(_._1 != input.hero.id)
      .map { id => hero(board.size, depthCount, mylife, heroStats(id._1)._1, heroStats(id._1)._2, positionByTavern(id._3), mymines) / (if (id._2) (depthCount + 1) else depthCount * 2 + 1) }.sum
  }

  def updateInput(): Input = {
    val newPossibleHeroPositions: Map[Int, PossibleHero] = depthCount match {
      case 0 => board.tiles.zipWithIndex
        .filter { x => x._1.isInstanceOf[Hero] && x._1.asInstanceOf[Hero].id != input.hero.id }
        .map(x => (x._1.asInstanceOf[Hero], board.pos(x._2)))
        .flatMap(x => x._2.neighbors.map(n => (x._1, n)))
        .filter(x => x._2.isIn(board.size) && board.at(x._2).get == Air)
        .map { x => (x._2.x * board.size + x._2.y, PossibleHero(1, x._1.id)) }.toMap
      case x if (x >= 1 && x < 3) => board.tiles.zipWithIndex
        .filter { x => x._1.isInstanceOf[PossibleHero] && x._1.asInstanceOf[PossibleHero].id != input.hero.id }
        .map(x => (x._1.asInstanceOf[PossibleHero], board.pos(x._2)))
        .filter(_._1.level == depthCount)
        .flatMap(x => x._2.neighbors.map(n => (x._1, n)))
        .filter(x => x._2.isIn(board.size) && board.at(x._2).get == Air)
        .map { x => (x._2.x * board.size + x._2.y, PossibleHero(depthCount + 1, x._1.id)) }.toMap
      case _ => Map()
    }
    val tiles = board.tiles.zipWithIndex.map { x => newPossibleHeroPositions.getOrElse(x._2, x._1) }
    newPossibleHeroPositions.foreach { x => possibleHeroPositions += x._2.id -> (possibleHeroPositions(x._2.id) + board.pos(x._1)) }
    input.withGame(input.game.withBoard(Board(board.size, tiles)))
  }

}

object ScoreCalculator {
  def props(input: Input, mapper: ActorRef, movement: Dir) = Props(new ScoreCalculator(input, mapper, movement))
  def props(input: Input, mapper: ActorRef, movement: Dir, depth: Option[(Int, Dir, Set[Pos], Pos, Int, Map[Int, Set[Pos]])]) = Props(new ScoreCalculator(input, mapper, movement, depth))

  def hero(size: Int, distance: Int, myhealth: Int, hisHealth: Int, mines: Int, nextToTavern: Boolean, mymines: Int): Int = {
    val hisCheckedHealth = hisHealth + (if (nextToTavern) 50 else 0) min 100
    myhealth - hisCheckedHealth match {
      case x if (hisHealth <= 20 && distance <= 1)                           => dF(size, 0) * 600 toInt
      case x if (x > 0 && myhealth > 21 && distance <= 1 && mines > mymines) => dF(size, 0) * 150 toInt
      case x if (x > 20 && mines > 0 && hisCheckedHealth <= 20)              => dF(size, distance) * 25 * mines toInt
      case x if (x > 20 && mines > 0 && hisCheckedHealth < 50)               => dF(size, distance) * 15 * mines toInt
      case x if (x > 0 && mines > 0 && hisCheckedHealth < 50)                => dF(size, distance) * 10 * mines toInt
      case x if (x < 0 && myhealth <= 20)                                    => -dF(size, distance) * 750 toInt
      case x if (x < 0 && myhealth < 25)                                     => -dF(size, distance) * 250 toInt
      case x if (x < -20 && myhealth < 50)                                   => -dF(size, distance) * 100 toInt
      case x if (x < -20)                                                    => -dF(size, distance) * 50
      case x if (x < 0 && myhealth < 50)                                     => -dF(size, distance) * 25 toInt
      case _                                                                 => 0
    }
  }

  def mine(size: Int, distance: Int, owned: Int, myId: Int, health: Int, minecount: Int) = owned match {
    case 0 if health > 70                                => dF(size, distance) * 100 toInt
    case 0 if health > 50                                => dF(size, distance) * 20 toInt
    case 0 if health > 21                                => dF(size, distance) toInt
    case 0 if health <= 21 && distance == 0              => -dF(size, distance) * 100 toInt
    case x if x != myId && health > 70                   => dF(size, distance) * 150 toInt
    case x if x != myId && health > 50                   => dF(size, distance) * 35 toInt
    case x if x != myId && health > 21                   => dF(size, distance) * 2 toInt
    case x if x != myId && health <= 21 && distance == 0 => -dF(size, distance) * 100 toInt
    case _                                               => 1
  }

  def tavern(size: Int, distance: Int, health: Int) = health match {
    case x if x >= 90 => 0
    case x if x >= 80 => 2 * dF(size, distance) toInt
    case x if x >= 70 => 5 * dF(size, distance) toInt
    case x if x >= 60 => 10 * dF(size, distance) toInt
    case x if x >= 50 => 15 * dF(size, distance) toInt
    case x if x >= 40 => 30 * dF(size, distance) toInt
    case x if x >= 20 => 70 * dF(size, distance) toInt
    case x            => 500 * dF(size, distance) toInt
  }

  def dF(size: Int, distance: Int) = distance match {
    case 0  => 5000
    case 1  => 1500
    case 2  => 1000
    case 3  => 750
    case 4  => 600
    case 5  => 500
    case 6  => 400
    case 7  => 350
    case 8  => 300
    case 9  => 250
    case 10 => 220
    case 11 => 195
    case 12 => 170
    case 13 => 150
    case 14 => 130
    case 15 => 115
    case 16 => 100
    case 17 => 90
    case 18 => 80
    case 19 => 70
    case 20 => 60
    case 21 => 55
    case 22 => 50
    case 23 => 45
    case 24 => 40
    case 25 => 35
    case 26 => 31
    case 27 => 27
    case 28 => 24
    case 29 => 21
    case 30 => 18
    case 31 => 15
    case 32 => 13
    case 33 => 11
    case 34 => 9
    case 35 => 7
    case 36 => 6
    case 37 => 5
    case 38 => 4
    case 39 => 3
    case 40 => 2
    case _  => 1
  }

  def distances(from: Pos, to: Pos) = (Math.abs(from.x - to.x) + Math.abs(from.y - to.y)) toInt
}