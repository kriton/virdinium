package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile._
import scala.util.Random
import akka.actor.ActorRef

class SWBot(input: Input) extends Actor {
  import SWBot._
  import context._

  var moveHistory = Seq[Dir.Dir]()
  var requester: ActorRef = null
  var turn = 0
  def receive = {
    case MoveCommand(input, timeLimit) => {
      requester = sender
      turn += 1
      actorOf(DecisionMaker.props(input, moveHistory, timeLimit), "decider"+turn)
    }
    case Decision(movement) => {
      moveHistory :+= movement
      requester ! movement
    }
  }

}

object SWBot {
  def props(input: Input) = Props(new SWBot(input))

  case class Decision(movement: Dir)
  case class MoveCommand(input: Input, timeLimit: Int)
}