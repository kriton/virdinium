package kriton.vindinium.actors

import akka.actor.Actor
import akka.actor.Props
import kriton.vindinium.Input
import kriton.vindinium.Dir
import kriton.vindinium.Dir._
import kriton.vindinium.Tile._
import scala.util.Random
import akka.actor.ActorRef
import kriton.vindinium.Server
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import scala.concurrent.duration._

class GameMatcher(server: Server, gameType: String = "training", games: Int = Int.MaxValue, turns: Int = 50) extends Actor {
  import GameMatcher._
  import context._

  var gamesStarted = 0
  println(s"New gamematcher started for $games $gameType games.")
  var timeout = 450
  var lastGame: ActorRef = null
  self ! NewGame

  def receive = {
    case NewGame          => newGame()
    case GameEnded(input) => finishGame(input);
  }

  def finishGame(input: Input) = {
    println(s"\n[$gamesStarted/$games] Finished game ${input.viewUrl}")
    stop(lastGame)
    self ! NewGame
  }

  def terminateCheck() = if (gamesStarted >= games) {
    println("All games have been played out!")
    println("Terminating!")
    system.terminate()
    true
  } else false

  def newGame() = if (!terminateCheck()) {
    gamesStarted += 1
    println(s"[$gamesStarted / $games] Starting new $gameType game!")
    if (gameType == "arena") println(s"Waiting for pairing...")
    lastGame = actorOf(GameStarter.props(server, gameType, turns, timeout), s"game$gamesStarted")
  }

  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
    case e: scalaj.http.HttpException => { println(s"\n[${e.code}] ${e.body}...\n"); scheduleNewGame(); Stop }
    case e: Exception                 => { scheduleNewGame(); Stop }
    case _                            => Stop
  }

  def scheduleNewGame() = {
    if (timeout <= 350) {
      println(s"Timeout is too low ($timeout)! Shutting down...")
      system.terminate()
    } else {
      timeout -= 5
      println(s"\nScheduling a new game with $timeout ms timeout ...\n");
      system.scheduler.scheduleOnce(5 seconds, self, NewGame);
    }
  }

}

object GameMatcher {
  def props(server: Server, gameType: String = "training", games: Int = Int.MaxValue, turns: Int = 50) = Props(new GameMatcher(server, gameType, games, turns))

  case class GameEnded(input: Input)
  case object NewGame
}