package kriton.vindinium

import scala.Left
import scala.Right
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.util.Timeout
import kriton.vindinium.actors.GameMatcher

object Main {

  implicit val timeout = Timeout(800 milliseconds)
  var bot: ActorRef = null
  val system = ActorSystem("vindinium")

  def main(args: Array[String]): Unit = makeServer match {
    case Left(error) ⇒ println(error)
    case Right(server) ⇒ args match {
      case Array() ⇒
        system.actorOf(GameMatcher.props(server, "arena"), "games")
      case Array("arena") ⇒
        system.actorOf(GameMatcher.props(server, "arena"), "games")
      case Array("arena", games) ⇒
        system.actorOf(GameMatcher.props(server, "arena", int(games)), "games")
      case Array("training") ⇒
        system.actorOf(GameMatcher.props(server, "training"), "games")
      case Array("training", turns) ⇒
        system.actorOf(GameMatcher.props(server, "training", Int.MaxValue, int(turns)), "games")
      case Array("training", turns, games) ⇒
        system.actorOf(GameMatcher.props(server, "training", int(games), int(turns)), "games")
      case Array("training", turns, map) ⇒
        println("not supported currentlny...")
      case a ⇒ println("Invalid arguments: " + a.mkString(" "))
    }
  }

  def makeServer = (
    Option(System.getProperty("server")) getOrElse "http://vindinium.org/",
    System.getProperty("key")) match {
      case (_, null)  ⇒ Left("Specify the user key with -Dkey=mySecretKey")
      case (url, key) ⇒ Right(new Server(url + "/api", key))
    }

  def int(str: String) = java.lang.Integer.parseInt(str)
}
