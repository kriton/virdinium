package kriton.vindinium

object Dir extends Enumeration {
  type Dir = Value
  val Stay, North, South, East, West = Value

  def oppositeOf(dir: Dir.Dir) = dir match {
    case Stay  => Stay
    case North => South
    case South => North
    case East  => West
    case West  => East
  }
}

case class Pos(x: Int, y: Int) extends Ordered[Pos] {
  import Dir._

  def neighbors = Set(North, South, West, East) map to

  def to(dir: Dir) = dir match {
    case Stay  ⇒ this
    case North ⇒ copy(x = x - 1)
    case South ⇒ copy(x = x + 1)
    case East  ⇒ copy(y = y + 1)
    case West  ⇒ copy(y = y - 1)
  }
  def isIn(size: Int) = (x >= 0 && x < size && y >= 0 && y < size)
  import scala.math.Ordered.orderingToOrdered

  def compare(that: Pos): Int = (this.x, this.y) compare (that.x, that.y)

}

object Pos {
  def normalize(from: Pos, to: Pos) = if (from < to) (from, to) else (to, from)
}

sealed trait Tile
object Tile {
  case object Air extends Tile { override def toString = "  " }
  case object Wall extends Tile { override def toString = "##" }
  case object Tavern extends Tile { override def toString = "[]" }
  case class Hero(id: Int) extends Tile { override def toString = s"@$id" }
  case class PossibleHero(level: Int, id: Int) extends Tile {
    override def toString = level match {
      case 1 => s"A$id"
      case 2 => s"B$id"
      case 3 => s"C$id"
      case 4 => s"D$id"
      case 5 => s"E$id"
    }
  }
  case class Mine(heroId: Option[Int]) extends Tile { override def toString = "$" + heroId.getOrElse("-") }
}

case class Board(size: Int, tiles: Vector[Tile]) {
  def pos(index: Int): Pos = Pos(index / size, index % size)
  def at(pos: Pos): Option[Tile] =
    if (pos isIn size) tiles lift (pos.x * size + pos.y) else None
}

case class Hero(
    id: Int,
    name: String,
    pos: Pos,
    life: Int,
    gold: Int,
    mineCount: Int,
    spawnPos: Pos,
    crashed: Boolean,
    elo: Option[Int]) {

  override def toString = s"Hero $id $pos life:$life mine:$mineCount gold:$gold"
}

case class Game(
    id: String,
    turn: Int,
    maxTurns: Int,
    heroes: List[Hero],
    board: Board,
    finished: Boolean) {
  def withBoard(newboard: Board) = Game(this.id, this.turn, this.maxTurns, this.heroes, newboard, this.finished)
}

case class Input(
    game: Game,
    hero: Hero,
    token: String,
    viewUrl: String,
    playUrl: String) {
  def withGame(newgame: Game) = Input(newgame, hero, token, viewUrl, playUrl)
}
